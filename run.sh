echo "Run setup.sh"
exit

cd PATH_TO_THIS_FILE

# Because IP can't be determined from docker container, we need to pass it in there
IP=`ifconfig -a eth0 |grep "inet " |cut -d ":" -f 2 | cut -d " " -f 1`

NAME=""
SERVER=""

# ASk settings at the first run.
if [ -f "NAME" ];
then
	NAME=`cat NAME`
else
	echo "Run setup.sh first."
	exit
fi

if [ -f "SERVER" ];
then
	SERVER=`cat SERVER`
else
	echo "Run setup.sh first."
	exit
fi

echo "Using settings [$SERVER] [$NAME] [$IP]"

docker run -d -e SERVER=$SERVER -e NAME=$NAME -e IP=$IP registry.gitlab.com/baze/report-my-ip-client
