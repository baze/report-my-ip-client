# Introduction
This application is used to track changing IP addresses of your Raspberry PI's.
This project consists of [Server](https://gitlab.com/baze/report-my-ip) and [Client](https://gitlab.com/baze/report-my-ip-client) application.

This is the **Client** part.

# Install and setup

Install client and run setup simply by running:
```
    curl -sSL https://gitlab.com/baze/report-my-ip-client/raw/master/setup.sh | sh
```

Setup will ask name for your client and ip/port of the central server. This client will start reporting its IP to central server.

Then you can place *run.sh* in your crontab as instructed by *setup.sh*.

# Start client
You can also run the client manually like this if you want to:
```
    ./run.sh
```

