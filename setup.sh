#!/bin/bash

# This script can be called directly from GIT so we download git content if needed
if [ ! -f "reportIp.sh" ];
then
	echo -n "Loading client..."
	git clone -q https://gitlab.com/baze/report-my-ip-client
	cd report-my-ip-client
	echo "done."
fi

PATHH=`pwd`

# ASk settings and store them to files
read -p "Name your client:" n < /dev/tty
echo $n > NAME

read -p "Give your servers address [name:ip]:" n < /dev/tty
echo $n > SERVER

echo "Hardcoding path[$PATHH] to run.sh"
echo ""

sed -i "s|PATH_TO_THIS_FILE|${PATHH}|g" run.sh
tail -n +3 run.sh > run.sh_temp
mv run.sh_temp run.sh
chmod u+x run.sh

# Ask if we should start the client right now
read -p "Start client now[y/n]:" yn < /dev/tty
case $yn in
  [Yy]* )
     echo "-------------";
     echo "Starting client...";
     sh run.sh;
     echo "done.";
     echo "-------------";;
esac

echo "Add next line to your crontab:"
echo "@reboot  $PATHH/run.sh"
echo ""
echo "Or run client manually \"./run.sh\""
