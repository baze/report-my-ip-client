
if [ -z $IP ];
then
	echo "[IP] environment variable not given. Exiting."
	exit
fi

if [ -z $SERVER ];
then
	echo "[SERVER] environment variable not given. Exiting."
	exit
fi

if [ -z $NAME ];
then
	echo "[NAME] environment variable not given. Exiting."
	exit
fi

echo "Server IP[$IP]"

while true;
do
	echo -n "reporting server ip[$IP][$NAME]."
	curl "http://$SERVER/?ip=$IP&name=$NAME"
	echo " Sleeping."
	sleep 480
done
